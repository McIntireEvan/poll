<?php 
    include 'templates/header.php';
    if(sizeof($_POST) != 0) {
        $multi = isset($_POST['multi']) ? true : false;
        $response = postPoll($_POST['title'], $multi, $_POST['data']);
        if(gettype($response) == 'array') {
            echo 'Created! Redirecting...';
            header('refresh:1;url=results?id=' . $response['id']);
            die();
        } else {
            echo $response;
            die();
        }
    }
?>

<form action='?' method='post' class='center center-block'>
    <input name='title' type='text' class='form-control' placeholder=<?php echo '"' . $QUESTION_PLACEHOLDER . '"'; ?> required/>
    <div id='options'>
        <input name='data[0]' class='form-control' type='text' placeholder=<?php echo '"' .
            $OPTION_PLACEHOLDER . '"'; ?> required />
        <input id='o1' name='data[1]' class='form-control' type='text' placeholder=<?php echo '"' .
            $OPTION_PLACEHOLDER . '"'; ?> required />
    </div>
    <div id='checkbox-multi'>
        <input type='checkbox' name='multi' value='true'/>
        <label for='multi'>Allow multiple responses?</label>
    </div>
    <input type='submit' class='btn btn-default btn-lg' value='Create'/>
</form>

<script>
    var options = 2;

    function addNew() {
        $('<input name="data['+ options  +']" type="text" class="form-control" placeholder="Option" />').on('input', function() {
            options++;
            if(options < 12)
                addNew();
            $(this).off();
        }).appendTo('#options');
    }

    $(document).ready(function() {
        $('#o1').on('input', function() {
            addNew();
            $(this).off();
        });
    });
</script>
<?php include 'templates/footer.php'?>
