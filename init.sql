CREATE TABLE IF NOT EXISTS polls(qId INTEGER PRIMARY KEY, title TEXT, multichoice TEXT, uniqueId TEXT);
CREATE TABLE IF NOT EXISTS responses(rId INTEGER PRIMARY KEY, response TEXT, votes INTEGER);
CREATE TABLE IF NOT EXISTS bridge(qId INTEGER REFERENCES polls, rId INTEGER REFERENCES responses);
