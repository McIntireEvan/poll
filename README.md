# poll
A basic poll app written in php

API
===

Get
---
````curl https://poll.evanmcintire.com/api?id=<ID>````

````curl -d '{"id":"<ID>"}' -X GET https://poll.evanmcintire.com/api````
  
Post
----
````curl -d '{ "title":"<TITLE>", "multi":"false", "data":["<CHOICE>", "<CHOICE>", ...] }' https://poll.evanmcintire.com/api````
