<?php
    include 'vars.php';

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        if(gettype($result) == "array") {
            header('Content-Type: application/json');
            echo json_encode($result, JSON_PRETTY_PRINT);
        } else {
            echo $result;
            http_response_code(400);
        }
    } else if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $data = json_decode(file_get_contents('php://input'), true);

        if(isset($data['title']) && isset($data['data'])) {
            if(sizeof($data['data']) >= 2) {
                $multi = isset($data['multi']) ? true : false;
                $response = postPoll($data['title'], $multi, $data['data']);
                if(gettype($response) == "array") {
                    header('Content-Type: application/json');
                    echo json_encode($response);
                    http_response_code(201);
                } else {
                    http_response_code(400);
                }
            }
        } else {
            http_response_code(400);
        }
    }
?>
