<?php
    $DB_FILE = 'poll.db';
    $SITE_NAME = 'Poll';
    $QUESTION_PLACEHOLDER = 'Type your question here';
    $OPTION_PLACEHOLDER = 'Option';

    $db = new SQLite3($DB_FILE);
    function getPoll( $id ) {
        global $db;
        $result = $db->query('SELECT * FROM polls JOIN bridge USING ( qId ) JOIN responses USING ( rId );');
        if($result) {
            $rows = array();
            $return = array();
            $option = 0;
            $total = 0;
            while($rows = $result->fetchArray(SQLITE3_ASSOC)) {
                if($rows['qId'] == $id) {
                    $return['title'] = $rows['title'];
                    $return['multi'] = $rows['multichoice'];
                    $return['uniqueId'] = $rows['uniqueId'];
                    $return[$option] = array(
                        'id' => $rows['rId'],
                        'value' => $rows['response'],
                        'votes' => $rows['votes']
                    );
                    $total += $rows['votes'];
                    $option++;
                }
            }
            if(sizeOf($return) == 0) {
                return 'Poll does not exist';
            } else {
                $return['total'] = $total;
                return $return;
            }
        }
        return 'Error retrieving data';
    }

    function postPoll($title, $multi, $data) {
        global $db;
        if(isset($title)) {
            foreach($data as $val) {
                if(strlen($val) > 128) {
                    //TODO: Improve this
                    return 'Error: Something was too big. Try remaking it with non-absurd values';
                }
            }
            $statement = $db->prepare("INSERT INTO polls(title, multichoice, uniqueId) VALUES (?,?,?);");
            $statement->bindValue('1', htmlspecialchars($title));
            //Todo: Fix?
            if($multi) {
                $statement->bindValue('2', 'true');
            } else {
                $statement->bindValue('2', 'false');
            }
            $uId = uniqid();
            $statement->bindValue(3, $uId);
            $statement->execute();

            $qId = $db->lastInsertRowid();
            $P = array_values($data);
            for($i = 0; $i < sizeOf($P); $i++) {
                if($P[$i] != '') {
                    $statement = $db->prepare("INSERT INTO responses(response, votes) VALUES ( ?, ?);");
                    $statement->bindValue('1', htmlspecialchars($P[$i]));
                    $statement->bindValue('2', 0);
                    $statement->execute();

                    $rId = $db->lastInsertRowid();

                    $statement = $db->prepare("INSERT INTO bridge(qId, rId) VALUES (?, ?);");
                    $statement->bindValue('1', $qId);
                    $statement->bindValue('2', $rId);
                    $statement->execute();
                }
            }
            return array("id" => $qId . '-' . $uId);
        }
    }

    function check_exists($id) {
        if(isset($id)) {
            $num = split('-', $id)[0];
            $uId = split('-', $id)[1];
            $result = getPoll($num);
            if(gettype($result) == "array") {
                if($result['uniqueId'] == $uId) {
                    return $result;
                } else {
                    return "Poll with id {$id} not found";
                }
            } else {
                return $result;
            }
        } else {
            return 'No id specified';
        }
    }
    $result = '';
    if(isset($_GET['id'])) {
        $result = check_exists($_GET['id']);
    } else {
        $g = json_decode(file_get_contents('php://input'), true);
        if(isset($g['id'])) {
            $result = check_exists($g['id']);
        } else {
            $result = 'No id specified';
        }
    }
?>
