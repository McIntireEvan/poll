<?php 
    include 'templates/header.php';
    if(gettype($result) == "array") {
        $total = $result['total'];
        //TODO: Improve the formatting here
        echo '<div class="row center-block"><div class="col-md-6">'
           . '<table class="table">'
           . "<thead><th><h1>{$result['title']}</h1></th></thead>";
            for($i = 0; $i<(sizeOf($result) - 4); $i++) {
                $current = $result[$i];
                $percent = $total != 0 ? round(($current['votes']/$total)*100) : 0;
                echo "<tr id='r{$i}'><td>{$current['value']}</td>"
                   . "<td><span id='{$current['id']}-value'> {$current['votes']} '</span>"
                   . "(<span id='{$current['id']}-percent'> {$percent} </span>%)</td></tr>";
            }
        echo '</table></div><div class="col-md-6"><canvas id="pie" width=400 height=400></canvas></div></div>';
        echo "<a class='btn btn-default' href='vote?id={$_GET['id']}'> Vote </a>";
    } else {
        echo $result;
        die();
    }
?>

<script type='text/javascript'>
    function drawGraph(data, total, radius) {
        var canvas = $('#pie').get(0);
        var ctx = canvas.getContext('2d');
        var width = canvas.width;
        var height = canvas.height;
        var last = 0;
        var colors = ['#E25668','#E28956','#E2CF56','#AEE256',
                      '#68E256','#56E289','#56E2CF','#56AEE2',
                      '#5668E2','#8A56E2','#CF56E2','#E256AE'];

        for(var i = 0; i < data.length; i++) {
            ctx.beginPath();
            ctx.fillStyle = colors[i];
            ctx.moveTo(width/2,height/2);
            ctx.arc(width/2, height/2, radius, last -.01, last + ((data[i]/total) * 2 * Math.PI), false);
            ctx.lineTo(width/2, height/2);
            ctx.fill();
            last += (data[i]/total) * 2 * Math.PI;
        }
    }

    $(document).ready(function() {
        function update() {
            $.get(
                'api.php',
                {
                    id: <?php echo "\"{$_GET['id']}\""; ?>
                },
                function(data) {
                    var votes = [];
                    $.each(data, function(i, obj) {
                       if(typeof(obj) == 'object') {
                           $('#' + obj['id'] + '-value').html(obj['votes']);
                           $('#' + obj['id'] + '-percent').html(
                               Math.round((obj['votes']/data['total'])*100)
                           );
                           votes.push(obj['votes']);
                       }
                    });
                    drawGraph(votes, data['total'],200);
                }
            )
        }
        update();
        setInterval(update, 5000);
    });
</script>

<?php
    include 'templates/footer.php';
?>
