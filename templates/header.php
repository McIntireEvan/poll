<!doctype HTML>

<html>
    <head>
        <title> Poll </title>
        <meta name="viewport" content="width=device-width" />
        <link rel='shortcut icon' href='logo.png' type='image/x-icon' />
        <link rel='icon' type='image/png' href='logo.png' />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel='stylesheet' href='style.css'/>

        <?php
            include 'vars.php';
       ?>
    </head>
    <body>
        <h1 id='header'><a href='/'> <?php echo $SITE_NAME; ?> </a></h1>
