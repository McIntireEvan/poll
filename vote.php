<?php
include 'templates/header.php';
    echo '<div class="center center-block">';
    if(isset($_POST['choice']) || sizeof($_POST) > 0) {
        if(!isset($_COOKIE[$_GET['id']])) {
            if($result['multi'] == 'true') {
                $P = array_values($_POST);

                for($i = 0; $i < sizeof($P); $i++) {
                    $statement = $db->prepare('UPDATE responses SET votes = votes + 1 WHERE rId = ?');
                    $statement->bindValue('1', $P[$i]);
                    $statement->execute();
                }
            } else {
                $statement = $db->prepare('UPDATE responses SET votes = votes + 1 WHERE rId = ?');
                $statement->bindValue('1', $_POST['choice']);
                $statement->execute();
            }
            setcookie($_GET['id'], 'voted');
            echo 'Voted! Redirecting to results...';
        } else {
            echo 'Already voted on this poll! Redirecting to results...';
        }
        header( "refresh:1;url=results?id=" . $_GET['id'] );
        die();

    }
    //TODO: Improve styling here
    echo '<form action="#" method="POST">';

    if(gettype($result) == "array") {
        echo '<h1>' . $result['title'] . '</h1>';
        $multi = $result['multi'] == 'true';

        $type = $multi ? 'checkbox' : 'radio';
        $required = $multi ? '' : 'required';

        for($i = 0; $i<(sizeOf($result) - 4); $i++) {
            $name = $multi ? 'choice' . $i : 'choice';
            echo "<div class='{$type}'>"
                 . "<input type='{$type}'  name='{$name}' value='{$result[$i]['id']}' {$required}/>"
                 . $result[$i]['value'] . ' </div>';
        }
    } else {
        echo $result;
        die();
    }

    echo '<input class="btn btn-default btn-lg" type="submit"/>'
       . '</form>'
       . '<a href="results.php?id=' . $_GET['id'] . '"> Results </a>'
       . '</div>';
    include 'templates/footer.php';
?>
